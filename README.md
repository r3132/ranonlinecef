# RanOnlineCef

Implementing CEF for Ran UI

## Getting started

CEF is a multi-process app, it spawns another exe file (I call it a *UIHelper.exe* (1)) just to process logic, rendering, basically the entire browser capabilities. if you read this:

https://bitbucket.org/chromiumembedded/cef/wiki/GeneralUsage <-- **VERY IMPORTANT TO READ**

You will know that the CEF process spawns at least 4 process for different purposes.

The implementation is written with C++11 standards. The **CefClient** sample app from CEF has a **libcef_dll_wrapper** that is added to RAN's code base along with a **UIHelper.exe** from the General Usage (**Separate Sub-Process Executable**) for handling the processes that CEF needs.

## First Task ##
First download the CEF binaries from their automated builds: 
https://cef-builds.spotifycdn.com/index.html#windows32:90.6.5%2Bg7a604aa%2Bchromium-90.0.4430.93

- version used: **90.6.5+g7a604aa+chromium-90.0.4430.93 -- May 6 2021** (Use their version filter to find)
- windows 32-bit --> standard distribution, debug symbols and release symbols <-- DL all
- saved it all in `C:\cef`
- Try to compile the **CefClient** sample app that they have in the standard distribution download _Go here for instructions under **Windows Build Step**: https://bitbucket.org/chromiumembedded/cef/wiki/Tutorial_
    * It's imporatant to check the `CMakeLists.txt` on the CEF standard distribution folder to generate the MSVC solution file
    * you can create a directory name `build` in the CEF folder
    * then run `cd build` and `cmake -G "Visual Studio 16 2019" -A Win32`
    * now you can open the solution file

- When you have the test applications from CEF's release compiled and running you may now be ready for the CEF implementation on your source code.

Let's see how serious you are on implementing it.. Goodluck!

### Footnotes: ###
1. This is the best implementation I can think of. If you read the General Usage from CEF, they require to spawn either a helper exe or the application(GameClient.exe) multiple times to start other processes
